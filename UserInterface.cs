﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoneBook
{
    class UserInterface
    {
        public static int GetUserInput(ref int currentCursor, int currentMenuLenght)  //Метод для обработки ввода с клавиатуры в меню
        {
            var userInput = Console.ReadKey().Key;    // Считывание нажатой клавиши                    
            int testInt = 0;
            int userInputInt = -2;
            if (int.TryParse(userInput.ToString().Substring(userInput.ToString().Length - 1), out testInt) && testInt<=currentMenuLenght)
            {
                int.TryParse(userInput.ToString().Substring(userInput.ToString().Length - 1), out userInputInt); // Запись нажатой кнопки в userInputInt если это цифра
                if (currentMenuLenght>10 && userInputInt<=currentMenuLenght/10)
                {
                    userInput = Console.ReadKey().Key;
                    if (int.TryParse(userInput.ToString().Substring(userInput.ToString().Length - 1), out testInt))
                    {
                        userInputInt = 10 * userInputInt + testInt;
                        if (userInputInt > currentMenuLenght-1)
                        {
                            userInputInt = -2;
                        }
                    }
                }
            }
            // --------------------------------------------Реакция программы на нажатую клавишу----------------------------------------------
            if (userInput == ConsoleKey.Escape) // Закрыть программу или текущее меню, если нажат Escape
            {
                userInputInt = -1;   // -1 в данном случае будет кодом для выхода для любого меню
            }
            switch (userInput)
            {
                case ConsoleKey.UpArrow:
                    if (currentCursor == 1)
                    {
                        currentCursor = currentMenuLenght;
                    }
                    else
                    {
                        currentCursor = currentCursor - 1;
                    }
                    break;
                case ConsoleKey.DownArrow:
                    if (currentCursor == currentMenuLenght)
                    {
                        currentCursor = 1;
                    }
                    else
                    {
                        currentCursor = currentCursor + 1;
                    }
                    break;
                case ConsoleKey.Enter:
                    userInputInt = currentCursor;
                    break;
            }
            return userInputInt;
        }
        public static void ShowMainMenu(int currentCursor)
        {
            for (int i = 0; i <= 6; i++)
            {
                if (i == currentCursor)
                    Console.Write("--> ");
                switch (i)
                {
                    case 0:
                        Console.WriteLine("Выберите желаемую операцию: \n");
                        break;
                    case 1:
                        Console.WriteLine("1 - Создание новой записи \n");
                        break;
                    case 2:
                        Console.WriteLine("2 - Редактирование записи \n");
                        break;
                    case 3:
                        Console.WriteLine("3 - Удаление записи \n");
                        break;
                    case 4:
                        Console.WriteLine("4 - Просмотр созданных записей \n");
                        break;
                    case 5:
                        Console.WriteLine("5 - Выход \n\n\n\n\n*ПРИМЕЧАНИЕ\nДля навигации в программе используйте стрелки Вверх/Вниз, Enter и Escape\nили вводите номер нужного меню с клавиатуры");
                        break;
                }
            }
        }                       //Метод для вывода главного меню
        static void ShowPhoneBook(int currentCursor)                                 //Метод для вывода всех имеющихся записей
        {
            if (Program.PhoneBook.Count == 0)
                Console.WriteLine("На данный момент в книге нет записей. Вернитесь в главное меню чтобы их внести");
            for (int i = 1; i <= Program.PhoneBook.Count; i++)
            {
                if (i == currentCursor)
                    Console.Write("--> ");
                Console.WriteLine((i) + ".  " + Program.PhoneBook[i - 1].ToString());
            }
            Console.WriteLine();
            if (currentCursor == Program.PhoneBook.Count + 1)
                Console.Write("--> ");
            Console.WriteLine("Назад");
        }
        public static void EditExistingNote(int currentCursor)
        {
            int userInputInt;
            while (true)
            {
                Console.Clear();
                if (Program.PhoneBook.Count != 0)
                {
                    Console.WriteLine("Выберите запись которую требуется отредактировать \n");
                }
                ShowPhoneBook(currentCursor);
                userInputInt = UserInterface.GetUserInput(ref currentCursor, Program.PhoneBook.Count + 1);
                if (userInputInt == -1 || userInputInt == Program.PhoneBook.Count + 1)
                {
                    currentCursor = 1;
                    break;
                }
                if (userInputInt > 0)
                {
                    currentCursor = 1;
                    int fixUserInput = userInputInt - 1;
                    while (true)
                    {
                        Console.Clear();
                        Program.PhoneBook[fixUserInput].ShowEditInfo(currentCursor);
                        if (currentCursor == 10)
                        {
                            Console.Write("--> ");
                        }
                        Console.WriteLine("0. Назад");
                        userInputInt = UserInterface.GetUserInput(ref currentCursor, 10);
                        if (userInputInt == 10 || userInputInt == -1 || userInputInt == 0)
                        {
                            currentCursor = 1;
                            break;
                        }
                        if (userInputInt > 0 && userInputInt != 10)
                        {
                            Console.Clear();
                            switch (userInputInt)
                            {
                                case 1:
                                    Console.Write("Введите новую фамилию: ");
                                    Program.PhoneBook[fixUserInput].Surname = Console.ReadLine();
                                    break;
                                case 2:
                                    Console.Write("Введите новое имя: ");
                                    Program.PhoneBook[fixUserInput].Name = Console.ReadLine();
                                    break;
                                case 3:
                                    Console.Write("Введите новое отчество: ");
                                    Program.PhoneBook[fixUserInput].MiddleName = Console.ReadLine();
                                    break;
                                case 4:
                                    Console.Write("Введите новый номер телефона: ");
                                    Program.PhoneBook[fixUserInput].PhoneNumber = Console.ReadLine();
                                    break;
                                case 5:
                                    Console.Write("Введите новую страну: ");
                                    Program.PhoneBook[fixUserInput].Country = Console.ReadLine();
                                    break;
                                case 6:
                                    Console.Write("Введите новую дату рождения: ");
                                    Program.PhoneBook[fixUserInput].SetDateOfBirth(Console.ReadLine());
                                    break;
                                case 7:
                                    Console.Write("Введите новое место работы: ");
                                    Program.PhoneBook[fixUserInput].jobOrganization = Console.ReadLine();
                                    break;
                                case 8:
                                    Console.Write("Введите новую должность: ");
                                    Program.PhoneBook[fixUserInput].jobPosition = Console.ReadLine();
                                    break;
                                case 9:
                                    Console.Clear();
                                    Console.Write("Введите новое примечание: ");
                                    Program.PhoneBook[fixUserInput].other = Console.ReadLine();
                                    break;
                            }
                        }
                    }
                }
            }
        }                   //Метод для редактирования существующих записей
        public static void DeleteExistingNote(int currentCursor)
        {
            int userInputInt;
            while (true)
            {
                Console.Clear();
                if (Program.PhoneBook.Count != 0)
                {
                    Console.WriteLine("Выберите запись которую требуется удалить \n");
                }
                ShowPhoneBook(currentCursor);
                userInputInt = UserInterface.GetUserInput(ref currentCursor, Program.PhoneBook.Count + 1);
                if (userInputInt == -1 || userInputInt == Program.PhoneBook.Count + 1)
                {
                    currentCursor = 1;
                    break;
                }
                if (userInputInt > 0)
                {
                    Console.WriteLine("Вы уверены что хотите удалить запись - " + Program.PhoneBook[userInputInt - 1].ToString());
                    Console.Write("Y / N   ");
                    ConsoleKey YorN;
                    while (true)
                    {
                        YorN = Console.ReadKey().Key;
                        Console.WriteLine("");
                        if (YorN.ToString() == "Y")
                        {
                            Program.PhoneBook.RemoveAt(userInputInt - 1);
                            break;
                        }
                        else if (YorN.ToString() == "N")
                        {
                            break;
                        }
                        else
                        {
                            Console.WriteLine("Нажмите на кнопку Y или N для подтверждения или отмены удаления: ");
                        }
                    };
                }
            }
        }                 // Метод для удаления существующих записей
        public static void ShowExistingNote(int currentCursor)
        {
            int userInputInt;
            while (true)
            {
                Console.Clear();
                if (Program.PhoneBook.Count != 0)
                {
                    Console.WriteLine("Выберите интересующую вас запись \n");
                }
                ShowPhoneBook(currentCursor);
                userInputInt = UserInterface.GetUserInput(ref currentCursor, Program.PhoneBook.Count + 1);
                if (userInputInt == -1 || userInputInt == Program.PhoneBook.Count + 1)
                {
                    currentCursor = 1;
                    break;
                }
                if (userInputInt > 0)
                {
                    currentCursor = 1;
                    int fixUserInput = userInputInt - 1;
                    while (true)
                    {
                        Console.Clear();
                        Program.PhoneBook[fixUserInput].ShowFullInfo();
                        Console.WriteLine("\n --> Назад");
                        userInputInt = UserInterface.GetUserInput(ref currentCursor, 1);
                        if (userInputInt == 1 || userInputInt == -1)
                            break;
                    }
                }
            }
        }                   // Метод для просмотре существующих записей
    }

}
