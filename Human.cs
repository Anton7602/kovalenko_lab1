﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoneBook
{
    class Human
    {
        static int counter = 0;
        int id;
        public string Name { get { return name; } set { CheckNotZero(ref value); CheckValid(ref value); name = value; } }
        string name;
        public string MiddleName { get { return middleName; } set { CheckValid(ref value); middleName = value; } }
        public string middleName;
        public string Surname { get { return surname; } set { CheckNotZero(ref value); CheckValid(ref value); surname = value; } }
        string surname;
        public string PhoneNumber { get { return phoneNumber; } set { CheckNotZero(ref value); CheckValidNum(ref value); ; phoneNumber = value; } }
        string phoneNumber;
        public string Country { get { return country; } set { CheckNotZero(ref value); CheckValid(ref value); ; country = value; } }
        string country;
        DateTime dateofBirth;
        public string jobOrganization { get; set; }
        public string jobPosition { get; set; }
        public string other { get; set; }
        public Human()                                                       
        {
            Console.Clear();
            this.id = Human.counter;
            Human.counter++;
            Console.Write("Фамилия: ");
            this.Surname=Console.ReadLine();
            Console.Write("Имя: ");
            this.Name=Console.ReadLine();
            Console.Write("Отчество: ");
            this.MiddleName=Console.ReadLine();
            Console.Write("Номер телефона: ");
            this.PhoneNumber=Console.ReadLine();
            Console.Write("Страна: ");
            this.Country=Console.ReadLine();
            Console.Write("Дата рождения (дд.мм.гггг): ");
            SetDateOfBirth(Console.ReadLine());
            Console.Write("Место работы: ");
            this.jobOrganization=Console.ReadLine();
            Console.Write("Должность: ");
            this.jobPosition=Console.ReadLine();
            Console.Write("Примечания: ");
            this.other=Console.ReadLine();
         }
        public override string ToString()                                   
        {
            return $"{this.surname} {this.name} {this.phoneNumber}";
        }
        private void CheckNotZero(ref string tempStr)                       // проверка что введённая строка не пустая
        {
            while (tempStr.Length == 0)
            {
                Console.Write("Введённая строка пуста, пожалуйста введите корректное значение: ");
                tempStr = Console.ReadLine();
                Console.WriteLine();
            }
        }
        private void CheckValid(ref string tempStr)                         // Проверка что в введённой строке нет цифр
        {
            bool hasDigits;
            int tempInt;
            do
            {
                hasDigits = false;
                for (int i = 0; i < tempStr.Length; i++)
                {
                    if (int.TryParse(tempStr.Substring(i, 1), out tempInt))
                    {
                        Console.Write("Эта строка не должна содержать цифры. Пожалуйста введите корректное значение: ");
                        hasDigits = true;
                        tempStr = Console.ReadLine();
                        Console.WriteLine();
                        break;
                    }
                }
            }
            while (hasDigits == true);
            if (tempStr.Length != 0)
            {
                tempStr = tempStr.Substring(0, 1).ToUpper() + tempStr.Substring(1, tempStr.Length - 1); // Перевод первого символа в заглавный
            }
        }
        private void CheckValidNum(ref string tempStr)                      // Проверка что в ведённой строке только цифры
        {
            bool NotOnlyDigits;
            int tempInt;
            do
            {
                NotOnlyDigits = false;
                for (int i = 0; i < tempStr.Length; i++)
                {
                    if (!(int.TryParse(tempStr.Substring(i, 1), out tempInt)||(tempStr.Substring(i,1)=="+"&& i==0)))
                    {
                        Console.Write("Эта строка должна содержать только цифры(Первый символ может быть +). Пожалуйста, введите корректное значение: ");
                        NotOnlyDigits = true;
                        tempStr = Console.ReadLine();
                        Console.WriteLine();
                        break;
                    }
                }
                if (tempStr.Length==1 && tempStr=="+")
                {
                    Console.WriteLine("Строка не может состоять только из знака +. Пожалуйста, введите корректное значение");
                    NotOnlyDigits = true;
                    tempStr = Console.ReadLine();
                    Console.WriteLine();
                }
            }
            while (NotOnlyDigits == true);
        }
        public void SetDateOfBirth(string tempStr)                          // Проверка строки и присовение даты рождения
        {
            if (tempStr.Length != 0)
            {
                CheckCorrectDateFormat(ref tempStr);
                int year = int.Parse(tempStr.Substring(6, 4));
                int month = int.Parse(tempStr.Substring(3, 2));
                int day = int.Parse(tempStr.Substring(0, 2));
                this.dateofBirth = new DateTime(year, month, day);
            }
        }
        private DateTime CheckCorrectDateFormat(ref string tempStr)         //Проверка что дата введена корректно
        {
            bool isDateCorrect;
            DateTime Date = new DateTime();
            int year = 0;
            int month = 0;
            int day = 0;
            do
            {
                isDateCorrect = true;
                int forParse;
                if (tempStr.Length != 10 || tempStr.Substring(2, 1) != "." || tempStr.Substring(5, 1) != ".")
                {
                    Console.WriteLine("Неверный формат ввода даты. Пожалуйста, введите дату в формате дд.мм.гггг: ");
                    isDateCorrect = false;
                    tempStr = Console.ReadLine();
                    continue;
                }
                if (int.TryParse(tempStr.Substring(6, 4), out forParse) && forParse > 0)
                {
                    year = int.Parse(tempStr.Substring(6, 4));
                }
                else
                {
                    Console.WriteLine("Год введён некорректно. Пожалуйста, введите корректную дату: ");
                    isDateCorrect = false;
                    tempStr = Console.ReadLine();
                    continue;
                }
                if (int.TryParse(tempStr.Substring(3, 2), out forParse) && forParse <= 12 && forParse > 0)
                {
                    month = int.Parse(tempStr.Substring(3, 2));
                }
                else
                {
                    Console.WriteLine("Месяц введён некорректно. Пожалуйста, введите корректную дату: ");
                    isDateCorrect = false;
                    tempStr = Console.ReadLine();
                    continue;
                }
                if (int.TryParse(tempStr.Substring(0, 2), out forParse) && IsDayCorrect(forParse, month, year))
                {
                    day = int.Parse(tempStr.Substring(0, 2));
                }
                else
                {
                    Console.WriteLine("День введён некорректно. Пожалуйста, введите корректную дату: ");
                    isDateCorrect = false;
                    tempStr = Console.ReadLine();
                    continue;
                }
                Date = new DateTime(year, month, day);
                if (Date > DateTime.Now)
                {
                    Console.WriteLine("Неверный формат ввода даты. Введённая дата больше текущей. Пожалуйста, введите корректную дату");
                    isDateCorrect = false;
                    tempStr = Console.ReadLine();
                    continue;
                }

            }
            while (isDateCorrect == false);
            return Date;
        }
        public bool IsDayCorrect(int day, int month, int year)                // Проверка что день соответствует месяцу и году
        {
            int[] MonthWith31 = new int[] { 1, 3, 5, 7, 8, 10, 12 };
            int[] MonthWith30 = new int[] { 4, 6, 9, 11 };
            if (MonthWith31.Contains(month) && day <= 31 && day > 0)
                return true;
            if (MonthWith30.Contains(month) && day <= 30 && day > 0)
                return true;
            if (month == 2 && DateTime.IsLeapYear(year) && day <= 29 && day > 0)
                return true;
            if (month == 2 && !DateTime.IsLeapYear(year) && day <= 28 && day > 0)
                return true;
            return false;

        }
        public void ShowFullInfo()                                           // Вывод всей информации о выбранной записи
        {
            Console.WriteLine("Данные о контакте:\n\n");
            Console.WriteLine($"Фамилия: {this.surname}\n");
            Console.WriteLine($"Имя: {this.name}\n");
            Console.WriteLine($"Отчество: {this.middleName}\n");
            Console.WriteLine($"Номер телефона: {this.phoneNumber}\n");
            Console.WriteLine($"Страна: {this.country}\n");
            DateTime compareDate = new DateTime(1, 1, 2);
            if (this.dateofBirth > compareDate)
            {
                Console.WriteLine($"Дата рождения: {this.dateofBirth.ToShortDateString()}\n");
            }
            else
            {
                Console.WriteLine($"Дата рождения: \n");
            }
            Console.WriteLine($"Место работы: {this.jobOrganization}\n");
            Console.WriteLine($"Должность: {this.jobPosition}\n");
            Console.WriteLine($"Примечания: {this.other}\n");
        }
        public void ShowEditInfo(int currentCursor)                         // Вывод всех полей записи с поддержкой курсора
        {
            Console.WriteLine("Выберите поле, которое хотите редактировать:\n\n");
            for (int i = 1; i < 10; i++)
            {
                if (i == currentCursor)
                {
                    Console.Write("--> ");
                }
                switch (i)
                {
                    case 1:
                        Console.WriteLine($"1. Фамилия: {this.surname}\n");
                        break;
                    case 2:
                        Console.WriteLine($"2. Имя: {this.name}\n");
                        break;
                    case 3:
                        Console.WriteLine($"3. Отчество: {this.middleName}\n");
                        break;
                    case 4:
                        Console.WriteLine($"4. Номер телефона: {this.phoneNumber}\n");
                        break;
                    case 5:
                        Console.WriteLine($"5. Страна: {this.country}\n");
                        break;
                    case 6:
                        DateTime compareDate = new DateTime(1, 1, 2);
                        if (this.dateofBirth > compareDate)
                        {
                            Console.WriteLine($"6. Дата рождения: {this.dateofBirth.ToShortDateString()}\n");
                        }
                        else
                        {
                            Console.WriteLine($"6. Дата рождения: \n");
                        }
                        break;
                    case 7:
                        Console.WriteLine($"7. Место работы: {this.jobOrganization}\n");
                        break;
                    case 8:
                        Console.WriteLine($"8. Должность: {this.jobPosition}\n");
                        break;
                    case 9:
                        Console.WriteLine($"9. Примечания: {this.other}\n");
                        break;
                }
            }
        }

    }
}
