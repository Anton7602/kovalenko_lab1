﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoneBook
{
    class Program
    {
        static public List<Human> PhoneBook = new List<Human>();
 
        static void Main(string[] args)
        {
            int currentCursor=1; //Переменная curentCursor отвечает за текущее положение курсора в меню
            while (true)
            {
                Console.Clear();
                UserInterface.ShowMainMenu(currentCursor);
                int userInputInt = UserInterface.GetUserInput(ref currentCursor, 5);
                if (userInputInt==5 || userInputInt==-1)
                {
                    break;
                }
                switch (userInputInt)
                {
                    case 1:                                             //Внесение новой записи в книгу
                        Human human = new Human();
                        PhoneBook.Add(human);
                        break;
                    case 2:                                             //Редактирование записи в книге
                        currentCursor = 1;
                        UserInterface.EditExistingNote(currentCursor);
                        break;
                    case 3:                                             //Удаление записи в книге
                        currentCursor = 1;
                        UserInterface.DeleteExistingNote(currentCursor);
                        break;
                    case 4:                                             //Просмотр записей в книге
                        currentCursor = 1;
                        UserInterface.ShowExistingNote(currentCursor);
                        break;
                }
            }
        }
    }
 }

